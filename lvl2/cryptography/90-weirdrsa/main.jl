# extended_gcd(a, b) -> (x, y) such that ax + by = gcd(a, b)
function extended_gcd{T <: Integer}(a::T, b::T)
  if mod(a, b) == zero(T)
    return [zero(T), one(1)]
  else
    x, y = extended_gcd(b, mod(a, b))
    return [y, x - (y * fld(a, b))]
  end
end

# modulo_inverse(a, n) -> b such that a * b = 1 [mod n]
function modulo_inverse(a::Integer, n::Integer)
  mod(first(extended_gcd(a, n)), n)
end

# square(x) = x^2
square(x::Integer) = x * x

# modulo_power(base, exp, n) -> base^exp [mod n]
function modulo_power{T <: Integer}(base::T, exp::T, n::T)
  if exp == zero(T)
    one(T)
  else
    if isodd(exp)
      mod(base * modulo_power(base, exp - one(T), n), n)
    else
      mod(square(modulo_power(base, fld(exp, oftype(exp, 2)), n)), n)
    end
  end
end

function chinese_alghoritm{T <: Integer}(c::T, p::T, q::T, dp::T, dq::T)
    qinv = modulo_inverse(q,p)
    m1 = modulo_power(c, dp, p)
    m2 = modulo_power(c, dq, q)
    if(m1<m2)
        while m1<m2
            m1 +=p
        end
    end
    h = (qinv*(m1-m2))%p
    return m2 + h*q
end


# ---
# RSA 
# ---

c = BigInt(95272795986475189505518980251137003509292621140166383887854853863720692420204142448424074834657149326853553097626486371206617513769930277580823116437975487148956107509247564965652417450550680181691869432067892028368985007229633943149091684419834136214793476910417359537696632874045272326665036717324623992885)
p = BigInt(11387480584909854985125335848240384226653929942757756384489381242206157197986555243995335158328781970310603060671486688856263776452654268043936036556215243)
q = BigInt(12972222875218086547425818961477257915105515705982283726851833508079600460542479267972050216838604649742870515200462359007315431848784163790312424462439629)
dp = BigInt(8191957726161111880866028229950166742224147653136894248088678244548815086744810656765529876284622829884409590596114090872889522887052772791407131880103961)
dq = BigInt(3570695757580148093370242608506191464756425954703930236924583065811730548932270595568088372441809535917032142349986828862994856575730078580414026791444659)


m = chinese_alghoritm(c, p, q,dp,dq)

#hex transformation
m = hex(m)
#separate two digits
m = [m[i:i+1] for i ∈ 1:2:length(m)-1]
#to ascii
m = [Char(parse(Int,n,16)) for n ∈ m]

#join all chars
decrypted_ciphertext = ""
for i ∈ m
    decrypted_ciphertext *= string(i)
end
@printf(
    "The decrypted ciphertext is: %s\n", decrypted_ciphertext)

