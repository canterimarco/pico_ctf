from sys import exit

c= 95272795986475189505518980251137003509292621140166383887854853863720692420204142448424074834657149326853553097626486371206617513769930277580823116437975487148956107509247564965652417450550680181691869432067892028368985007229633943149091684419834136214793476910417359537696632874045272326665036717324623992885
p= 11387480584909854985125335848240384226653929942757756384489381242206157197986555243995335158328781970310603060671486688856263776452654268043936036556215243
q= 12972222875218086547425818961477257915105515705982283726851833508079600460542479267972050216838604649742870515200462359007315431848784163790312424462439629
dp= 8191957726161111880866028229950166742224147653136894248088678244548815086744810656765529876284622829884409590596114090872889522887052772791407131880103961
dq= 3570695757580148093370242608506191464756425954703930236924583065811730548932270595568088372441809535917032142349986828862994856575730078580414026791444659

def modular_inverse(x, n):
    inv = 1
    total = b = x%n
    while total%n != 1:
        total += b
        inv += 1
    return inv

def modular_inverse_optim(a,n):
    # gcd(a,b) = a*x + b*y
    # if looking for a*a'=1 (mod n)
    # then gcd(a,n) = a*x + n*y = 1
    # so a' = x%n
    def extended_euclidean_gcd(a,b):
        # gcd = a*x + b*y
        # r0 = a*s0 + b*t0
        r0, s0, t0 = a, 1, 0
        r1, s1, t1 = b, 0, 1
        while r1!=0:
            quot = r0//r1
            r0,r1 = r1, r0-quot*r1
            s0,s1 = s1, s0-quot*s1
            t0,t1 = t1, t0-quot*t1
        return r0, s0, t0

    gcd, x, _ =  extended_euclidean_gcd(a,n)
    if gcd!=1:
        raise Exception('the number is not prime or modular invertible!')
    return x%n


def rsa_rct(c,p,q,dp,dq):
    m1 = pow(c,dp,p)
    m2 = pow(c,dq,q)
    if m1==m2:
        print('m1 is equal to m2????????')
        exit()
    qinv = modular_inverse_optim(q,p)
    h = (qinv*(m1-m2)) % p
    m = m2 + h*q
    for s in 'qinv m1 m2 h m'.split():
        print('%5s = %d' % (s,eval(s)))
    return m

################# chinese optimmization algorithm
m = rsa_rct(c,p,q,dp,dq)
################# to ascii
m = str(m)
m = [m[i:i+2] for i in range(0,len(m),2)]
m = [chr(int(n)) for n in m]
m = ''.join(m)
print(m)
