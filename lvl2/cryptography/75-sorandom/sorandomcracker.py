#!/usr/bin/python -u
import random,string

encflag = "BNZQ:449xg472190mwx6869b8pt10rwo92624"
decrypted = ""
random.seed("random")
for c in encflag:
  if c.islower():
    a = random.randrange(0,26)
    b = chr(ord(c)+26-a)
    if b.islower():
    	decrypted += chr(ord(c)+26-a)
    else:
    	decrypted += chr(ord(c)-a)
  elif c.isupper():
  	a = random.randrange(0,26)
  	b = chr(ord(c)+26-a)
  	if b.isupper():
  		decrypted += chr(ord(c)+26-a)
  	else:
  		decrypted += chr(ord(c)-a)
  elif c.isdigit():
    a = random.randrange(0,10)
    b = chr(ord(c)+10-a)
    if b.isdigit():
    	decrypted += chr(ord(c)+10-a)
    else:
    	decrypted += chr(ord(c)-a)
  else:
    decrypted += c
print "Flag: "+ decrypted



