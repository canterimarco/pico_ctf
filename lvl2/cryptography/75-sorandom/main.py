import random, sys, string
ciphertext = 'BNZQ:2m8807395d9os2156v70qu84sy1w2i6e'
random.seed('random')

result = []
for c in ciphertext:
    if c.islower():
        rand = random.randrange(0,26)
        base = ord('a')
        offset = ord(c)-base
        clearoff = offset - rand
        clear = string.ascii_lowercase[clearoff]
    elif c.isupper():
        rand = random.randrange(0,26)
        base = ord('A')
        offset = ord(c)-base
        clearoff = offset - rand
        clear = string.ascii_uppercase[clearoff]
    elif c.isdigit():
        rand = random.randrange(0,10)
        base = ord('0')
        offset = ord(c)-base
        clearoff = offset - rand
        clear = string.digits[clearoff]
    else:
        rand = 0
        base = 0
        offset = ord(c)
        clearoff = offset
        clear = chr(clearoff)
    result.append(clear)
print ''.join(result)
