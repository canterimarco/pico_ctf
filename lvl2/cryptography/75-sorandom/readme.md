A python script takes the flag and *randomizes* its characters, randomize it's a big word since at the beginnng of the code the function seed is invoked with a specific input. This means that all the numbers that will come out from the function random() can be predicted. So the only task of this challenge is to revert the code for restoring the original flag. The code is the following:

```python
#!/usr/bin/python -u
import random,string
flag = "FLAG:"+open("flag", "r").read()[:-1]
encflag = ""
random.seed("random")
for c in flag:
  if c.islower():
    #rotate number around alphabet a random amount
    encflag += chr((ord(c)-ord('a')+random.randrange(0,26))%26 + ord('a'))  #22
  elif c.isupper():
    encflag += chr((ord(c)-ord('A')+random.randrange(0,26))%26 + ord('A')) #2
  elif c.isdigit():
    encflag += chr((ord(c)-ord('0')+random.randrange(0,10))%10 + ord('0')) #5
  else:
    encflag += c
print "Unguessably Randomized Flag: "+encflag
```
We can notice that the math is almost the same for every case, so we only need to revert one expression, the others will be a copy-paste. The equation to solve is the following:
```
encflag = chr((ord(c)-ord('a')+random.randrange(0,26))%26 + ord('a'))

```
the inverse function of chr() is ord(), appling that on both side of equation and moving the last term give us

```
ord(encflag)- ord('a') = (ord(c)-ord('a')+random.randrange(0,26))%26

```

the modulus is non invertible, but we can make a guess, since in the equation: ord(c)-ord('a')+random.randrange(0,26) laids between 0 and 52, the integer ratio will be 0 or 1. Recalling that if A%B = C then A = C + Floor(A/B)B we can now easly solve the equation:
```
ord(c) = ord(encflag) + x*26-random

```
where x is 0 or 1, worth to notice it's that there's no more ord('a'), so it's even easier to copy paste this result for the other 2 cases. To distinguish whether
x is 0 or 1, we can check the result and see if it's the expected one. In this way we is simple to write a python script that returns the flag:
```
107bd559693aef6692e1ed55ebe29514

```
