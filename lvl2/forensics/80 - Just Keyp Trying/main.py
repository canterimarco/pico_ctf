import scapy.all as scapy
loads = [x.load for x in scapy.rdpcap('data/data.pcap')]
datas = [x[-8:] for x in loads]
# check keyboard/keypad page codes
d={'0x1': 'ERR', '0x11': 'n', '0x13': 'p', '0x15': 'r', '0x16': 's', '0x1a': 'w', '0x1e': '1', '0x20': '3', '0x21': '4', '0x22': '5', '0x27': '0', '0x2d': '-','0x2f': '[','0x30': ']','0x4': 'a','0x6': 'c','0x7': 'd','0x8': 'e','0x9': 'f','0xa': 'g', '0xf': 'l'}
d['0x0']='REV'
s=''.join(d[hex(x[2])] for x in datas if x[2]!=0)
print(s)
print ('fixed -> ',s.replace('-','_'))
