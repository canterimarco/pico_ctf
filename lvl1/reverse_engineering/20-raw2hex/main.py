import sys, binascii
filename = sys.argv[1]
f = open(filename,'br')
b = f.read()
b = binascii.hexlify(b)
s = str(b)[2:]
n = s.find('3a')  # 0x3a is :, and the format is flag:.....
print(s[n+2:])
