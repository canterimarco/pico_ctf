the challenge is straighforward, first we need to translate a binary number to ascii
![1.png](1.png)
then the ascii to hexadecimal and the hexadecimal to decimal
![2.png](2.png)
for these step just search on google "\* to \*".
The third level is slightly more difficult, we are given an hash function which sum all the bytes of a string and return the sum modulo 16. The simpliest string is a single character, in order to answer we have to look at the ascii table and search for a character whose decimal representation is a multiple of 16 plus the reminder *n*
![3.png](3.png)
The final level is a md5 hash that we need to crack, luckly it's a simple one, just searching on google md5 decrypter and we have the password immediately
![4.png](4.png)
flag
```  
605911fee24de43af8ebe50ec7d210ec
```  
