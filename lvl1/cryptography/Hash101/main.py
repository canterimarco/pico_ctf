def bin_to_ascii(s):
   tmp = (s[i:i+8] for i in range(0,len(s),8))
   ll = list(map(lambda x: chr(int(x,2)),tmp))
   return ''.join(ll)

import sys
a = sys.argv[1]
b = bin_to_ascii(a)
print('1 - binary to ascii: '+b)
c = hex(int(a,2))[2:]
d = int(a,2)
print('2 - to hex: '+c+' and int: '+str(d))
chosen = [chr(i) for i in range(16*4,16*5)]
e = chosen[int(input('what is the desired %? '))]
print('3 - simple hash: '+e)
