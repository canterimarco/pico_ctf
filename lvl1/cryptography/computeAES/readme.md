to decrypt an AES message we can simply use the command line with the following instruction
``` 
openssl enc -d -aes-128-ecb -in chiper.txt -out plain.txt -nopad -nosalt -K CDBF6FF2E198A3F056CDB868B9E9D8DA -base64 
``` 
where *CDBF6FF2E198A3F056CDB868B9E9D8DA* is the hexadecimal rapresentation of the base64 key. In *plain.tx* we'll find the flag:
```
flag{do_not_let_machines_win_68fa218c} 

``` 