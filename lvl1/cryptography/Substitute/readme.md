We need to decyper a text with unknown encryption, but the title suggests a simple letter substitution, indeed looking at the chiper text we can notice some common patterns, for example a recurring three letters world *MIT* which is probably the translation of *the*, or also there is often an apostrophe followed by an *L* which could be an *s*. We can continue to analyse the code manually, but online there are some great tools which perform a letter frequency analysis and can guess the right substition, this is easier longer is the text, so we shouldn't have problems here. We copy the text on https://www.guballa.de/substitution-solver and we retrive our flag
``` 
IFONLYMODERNCRYPTOWASLIKETHIS

```  
![decrypted.png](decrypted.png)
