we can create a ssh keys with the following command
```
ssh-keygen -t rsa -C "your_email@example.com"
``` 
now in the web console we type the following commands
``` 
mkdir .ssh & cd .ssh
nano authorized_keys
``` 
and we copy here the generated key. On the terminal now we can run
``` 
ssh username@shell2017.picoctf.com
``` 
and after a moment we have the flag

![flag](flag.png)
``` 
who_needs_pwords_anyways
``` 
