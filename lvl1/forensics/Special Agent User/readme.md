Again we are given a pcap file, but this time we have to find the broswer that the user is using to navigate. If we load the file into wireshark there are a lot of useless icmp/others packets, we can use a filter to display only http packets. In this way we can notice there are only 3 packets sent by the user, in 2 of them the user agent is wget, in the last one we can see this
![captured](packet.png)
so the flag is
```
MSIE 9.0

```

