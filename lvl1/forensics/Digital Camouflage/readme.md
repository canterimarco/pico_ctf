We are given a pcap file and we need to extract an encrypted password. Let's fire up wireshark and load the pcap file. First of all let's filter the packets and have a look at the http packets. We can see there're some pages trough the user navigated, the first one looks like this:
```html
<div class="content">
    <h1>Log In </h1>
    <form name="login" class="contentstuff" method="post" action="pages/main.html" onsubmit="modifyPass()">
    <table>
    .<tr>
        .<td>Username</td>
            <td><input type="text" name="userid"/></td>
        </tr>
        <tr>
        .<td>Password</td>
            <td><input type="password" name="pswrd"/></td>
        </tr>
    </table>

    <button type="submit">Submit</button>
.<input type="reset" value="Cancel"/>
    </form>
    
    <script>
function modifyPass(){
document.login.pswrd.value = btoa(document.login.pswrd.value);
}
.</script>
```
A classic html form plus a javascript function which takes the password and convert it to base64 before sending the form values to _pages/main.html_. Great, we found the encryption, now we need the password, scrool down to the packet with the htttp request of _/pages/main.html_ and we find this
![caputerd](captured.png)
if we decode the password we get the flag
```
R6AKNhWwo7
```